package ru.androiddevschool.roadrunner.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.roadrunner.RoadRunner;

/**
 * Created by 01k1402 on 04.04.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y){
        if (name!=null) RoadRunner.get().setScreen(name);
    }
}
