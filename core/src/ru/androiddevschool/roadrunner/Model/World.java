package ru.androiddevschool.roadrunner.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.HashSet;

import ru.androiddevschool.roadrunner.Utils.Assets;
import ru.androiddevschool.roadrunner.Utils.Values;

/**
 * Created by 01k1402 on 18.04.2017.
 */
public class World extends Stage {
    private Player player;
    private Ground ground;
    private HashSet<Obstacle> obstacles;
    private HashSet<Obstacle> clouds;
    private HashSet<Actor> toDelete;
    private boolean over;

    public World(Viewport viewport, Batch batch) {
        super(viewport, batch);
        TextureRegionDrawable[] frames = new TextureRegionDrawable[8];
        for (int i = 0; i < 8; i++)
            frames[i] = Assets.get().images.get(String.format("/Runner/run%d", i));
        player = new Player(new Animation<TextureRegionDrawable>(0.07f, frames));
        player.setPosition(Values.playerX, Values.ground);
        player.setSize(100, 100);
        addActor(player);

        ground = new Ground(Assets.get().images.get("/zemly/slice1").getRegion().getTexture());
        ground.setPosition(0, 0);
        ground.setTilesCount(20);
        addActor(ground);

        obstacles = new HashSet<Obstacle>();
        clouds = new HashSet<Obstacle>();
        toDelete = new HashSet<Actor>();
        over = false;
    }

    public void act(float delta){
        super.act(delta);
        for(Obstacle obstacle : obstacles){
            if (obstacle.getX() < player.getX() - Values.WORLD_WIDTH/2)
                toDelete.add(obstacle);
            if (obstacle.getRect().overlaps(player.getRect())) over = true;
        }
        for(Obstacle cloud : clouds){
            if (cloud.getX() < player.getX() - Values.WORLD_WIDTH/2)
                toDelete.add(cloud);
        }
        for(Actor actor : toDelete){
            obstacles.remove(actor);
            actor.remove();
        }
        toDelete.clear();
        ground.step(player.getX());
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        player.tap();
        return false;
    }
    public boolean touchUp (int screenX, int screenY, int pointer, int button) {
        player.drop();
        return false;
    }

    public Player getPlayer() {
        return player;
    }

    public void addObstacle(Obstacle obstacle){
        obstacles.add(obstacle);
        addActor(obstacle);
    }
    public void addCloud(Obstacle obstacle) {
        clouds.add(obstacle);
        addActor(obstacle);
    }

    public boolean isOver() {
        return over;
    }
}
