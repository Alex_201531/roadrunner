package ru.androiddevschool.roadrunner.Model;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import javax.swing.plaf.BorderUIResource;
import javax.xml.bind.ValidationEventLocator;

import ru.androiddevschool.roadrunner.Utils.Values;


/**
 * Created by 01k1402 on 18.04.2017.
 */
public class Player extends Image {
    private Animation<TextureRegionDrawable> animation;
    private float time;
    private Vector2 velocity;
    private boolean startFalling;
    private Rectangle rectangle;

    public Player(Animation<TextureRegionDrawable> animation) {
        super(animation.getKeyFrame(0));
        this.animation = animation;
        this.velocity = new Vector2(Values.runSpeed, 0);
        this.startFalling = false;
        this.rectangle = new Rectangle();
    }

    public void act(float delta) {
        if (getY() > Values.ground || velocity.y > 0) {
            velocity.y -= Values.g * delta;
        } else {
            velocity.y = 0;
            setY(Values.ground);
        }
        moveBy(velocity.x*delta, velocity.y * delta);

        time += delta;
        if (isJumping())
            setDrawable(animation.getKeyFrame(animation.getAnimationDuration() / 2));
        else
            setDrawable(animation.getKeyFrame(time, true));
        super.act(delta);
    }

    public void tap() {
        if (getY() == Values.ground) velocity.y = Values.jumpSpeed;
    }

    public void drop() {
        if (velocity.y > Values.minVelocity) velocity.y = Values.minVelocity;
    }

    private boolean isJumping() {
        return velocity.y != 0;
    }

    public Rectangle getRect(){
        rectangle.set(getX(), getY(), getWidth(), getHeight());
        return rectangle;
    }
}
