package ru.androiddevschool.roadrunner.Model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import ru.androiddevschool.roadrunner.Utils.Values;

/**
 * Created by 01k1402 on 18.04.2017.
 */
public class Ground extends Actor{
    Texture img;
    public Ground(Texture texture){
        this.img = texture;
        this.img.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
    }

    public void step(float playerX){
        if (playerX - Values.playerX > getX()+2*img.getWidth())
        moveBy(img.getWidth(), 0);
    }

    public void draw(Batch batch, float parentAlpha){
        batch.draw(img, getX(), getY(), 0, 0, (int)getWidth(), (int) getHeight());
    }

    public void setTilesCount(int i) {
        setSize(img.getWidth() * i, img.getHeight());
    }
}
