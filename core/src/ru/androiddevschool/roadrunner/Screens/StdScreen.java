package ru.androiddevschool.roadrunner.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

import ru.androiddevschool.roadrunner.Utils.Values;

/**
 * Created by 01k1402 on 14.03.2017.
 */
class StdScreen implements Screen {
    public OrthographicCamera stageCamera;
    public OrthographicCamera uiCamera;
    public OrthographicCamera staticBgCamera;
    public Stage stage; //сцена для актеров
    public Stage ui; //сцена для кнопок
    public Stage staticBg; //сцена для кнопок
    private InputMultiplexer multiplexer;
    public StdScreen(SpriteBatch batch){
        stageCamera = new OrthographicCamera();
        stageCamera.setToOrtho(false);
        stage = new Stage(new FitViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, stageCamera), batch);

        uiCamera = new OrthographicCamera();
        uiCamera.setToOrtho(false);
        ui = new Stage(new FitViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, uiCamera), batch);

        staticBgCamera = new OrthographicCamera();
        staticBgCamera.setToOrtho(false);
        staticBg = new Stage(new FitViewport(Values.WORLD_WIDTH, Values.WORLD_HEIGHT, staticBgCamera), batch);

    }

    protected void act(float delta){
        ui.act(delta);
        stage.act(delta);
    }

    protected void draw() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        staticBg.draw();
        stage.draw();
        ui.draw();
    }
    protected void postAct(){
    }

    @Override
    public void show() {
        multiplexer = new InputMultiplexer(); //во время показывания экрана устанавливаем обработчики ввода
        multiplexer.addProcessor(ui);
        multiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void render(float delta) {
        act(delta);
        postAct();
        draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().setScreenSize(width,height);
        staticBg.getViewport().setScreenSize(width,height);
        ui.getViewport().setScreenSize(width,height);
    }

    @Override
    public void pause(){
    }

    @Override
    public void resume(){
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
