package ru.androiddevschool.roadrunner;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.roadrunner.Screens.Menu;
import ru.androiddevschool.roadrunner.Screens.Help;
import ru.androiddevschool.roadrunner.Screens.Play;
import ru.androiddevschool.roadrunner.Utils.Assets;

public class RoadRunner extends Game {
	private static RoadRunner instance = new RoadRunner();
	private RoadRunner(){}
	public static RoadRunner get(){return instance;}

	@Override
	public void create() {
		batch = new SpriteBatch();
		screens = new HashMap<String, Screen>();
		screens.put("Menu", new Menu(batch));
		screens.put("Play", new Play(batch));
		screens.put("Help", new Help(batch));
		Assets.get().music.play();
		setScreen("Menu");
	}

	public void setScreen(String name){
		if(screens.containsKey(name)) setScreen(screens.get(name));
	}

	private HashMap<String, Screen> screens;
	private SpriteBatch batch;
}
