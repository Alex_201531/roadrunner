package ru.androiddevschool.roadrunner.Model;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

/**
 * Created by 01k1402 on 16.05.2017.
 */
public class Obstacle extends Image {
    private Rectangle rectangle;
    public Obstacle(TextureRegionDrawable img, float x, float y){
        super(img);
        rectangle = new Rectangle();
        setPosition(x, y, Align.center);
    }
    public Rectangle getRect(){
        rectangle.set(getX(), getY(), getWidth(), getHeight());
        return rectangle;
    }
}
