package ru.androiddevschool.roadrunner.Utils;

/**
 * Created by 01k1402 on 04.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 960;
    public static final float WORLD_HEIGHT = 540;
    public static final float jumpSpeed = 1200;
    public static final float runSpeed = 600;
    public static final float g = 3300;
    public static final float ground = 70;
    public static final float minVelocity = 300;
    public static final float playerX = 100;
}
