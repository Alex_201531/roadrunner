package ru.androiddevschool.roadrunner.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.roadrunner.Controller.MusicToggler;
import ru.androiddevschool.roadrunner.Controller.ScreenTraveler;
import ru.androiddevschool.roadrunner.Utils.Assets;
import ru.androiddevschool.roadrunner.Utils.Values;

/**
 * Created by 01k1402 on 04.04.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        Button button;
        Image img;
        Table layout = new Table();
        layout.setFillParent(true);
        //layout.setDebug(true);

        img = new Image(Assets.get().images.get("/fount/1_font"));
        img.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);
        stage.addActor(img);

        for (int i = 0; i < 6; i++) layout.add().height(0);
        layout.row();

        button = new TextButton("Играть", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("blue text"));
        button.addListener(new ScreenTraveler("Play"));
        layout.add(button).size(200).pad(20);

        button = new TextButton("Правила", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("blue text"));
        button.addListener(new ScreenTraveler("Help"));
        layout.add(button).size(200).pad(20);

        button = new TextButton("Музыка", (TextButton.TextButtonStyle) Assets.get().buttonStyles.get("button02"));
        button.addListener(new MusicToggler(button));
        layout.add(button).size(200).pad(20).row();


        ui.addActor(layout);
    }
}
