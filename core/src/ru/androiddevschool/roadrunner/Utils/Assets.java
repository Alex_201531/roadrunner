package ru.androiddevschool.roadrunner.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;
/**
 * Created by 01k1402 on 04.04.2017.
 */


public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initGraphics();
        initFonts();
        initStyles();
        initMusic();
    }

    private void initMusic() {
        music = Gdx.audio.newMusic(Gdx.files.internal("Music/00.mp3"));
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(file("/fonts/10530.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameters.size = 35;
        parameters.shadowColor = Color.BLUE;
        parameters.shadowOffsetX = 1;
        parameters.shadowOffsetY = 2;
        parameters.color = Color.LIGHT_GRAY;
        parameters.characters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        fonts.put("blue shaded", generator.generateFont(parameters));
        fonts.put("text", new BitmapFont());
    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("button01", new Button.ButtonStyle(
                        images.get("/button/button up music"),
                        images.get("/button/buttondown music"),
                        images.get("/button/button up cl")
                )
        );
        buttonStyles.put("button02",new TextButton.TextButtonStyle(
                        images.get("/button/button up music"),
                        images.get("/button/buttondown music"),
                        images.get("/button/button up cl"),
                        fonts.get("blue shaded")
                )
        );
        buttonStyles.put("exit", new ImageButton.ImageButtonStyle(
                        images.get("/button/red_boxCross"),
                        null,
                        null,
                        null,
                        null,
                        null
                )
        );

        buttonStyles.put("blue text", new TextButton.TextButtonStyle(
                        images.get("/button/button up"),
                        images.get("/button/button reverse"),
                        null,
                        fonts.get("blue shaded")
                )
        );

    }

    private void initGraphics() {
        images = new HashMap<String, TextureRegionDrawable>();
        loadFolder(file("/"), "");
        for(String name : images.keySet()) System.out.println(name);
    }

    public void loadFolder(FileHandle folder, String prefix) {
        for (FileHandle file : folder.list())
            if (file.isDirectory()) {
                loadFolder(file, prefix + "/" + file.name());
            } else {
                if (file.extension().equals("jpg") || file.extension().equals("png")) {
                    images.put(prefix + "/" + file.nameWithoutExtension(), makeDr(file));
                    //System.out.println(prefix + "/" + file.nameWithoutExtension());
                }
            }
    }

    private FileHandle file(String path) {
        return Gdx.files.local(path);
    }

    private TextureRegionDrawable makeDr(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, BitmapFont> fonts;
    public Music music;
}

