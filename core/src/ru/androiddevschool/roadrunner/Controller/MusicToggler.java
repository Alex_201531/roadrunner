package ru.androiddevschool.roadrunner.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.roadrunner.Utils.Assets;

/**
 * Created by Admin on 23.05.2017.
 */

public class MusicToggler extends ClickListener {
    Button button;

    public MusicToggler(Button button) {
        this.button = button;
    }

    public void clicked(InputEvent event, float x, float y) {
        if (!button.isChecked())
            Assets.get().music.play();
        else
            Assets.get().music.pause();
    }
}
