package ru.androiddevschool.roadrunner.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.roadrunner.Controller.ScreenTraveler;
import ru.androiddevschool.roadrunner.Utils.Assets;
import ru.androiddevschool.roadrunner.Utils.Values;

/**
 * Created by 01k1402 on 11.04.2017.
 */
public class Help extends StdScreen {
    public Help(SpriteBatch batch){
        super(batch);
        Button button;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;
        img = new Image(Assets.get().images.get("/fount/1_font"));
        img.setSize(Values.WORLD_WIDTH,Values.WORLD_HEIGHT);
        stage.addActor(img);

        button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("exit"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add().expandX();
        layout.add(button).row();
        layout.add().colspan(2).expand().row();



        ui.addActor(layout);

    }
}
