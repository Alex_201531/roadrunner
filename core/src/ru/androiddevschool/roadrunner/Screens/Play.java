package ru.androiddevschool.roadrunner.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;

import java.util.Random;

import ru.androiddevschool.roadrunner.Controller.ScreenTraveler;
import ru.androiddevschool.roadrunner.Model.Obstacle;
import ru.androiddevschool.roadrunner.Model.World;
import ru.androiddevschool.roadrunner.Utils.Assets;
import ru.androiddevschool.roadrunner.Utils.Values;

/**
 * Created by 01k1402 on 11.04.2017.
 */
public class Play extends StdScreen {
    private static Random r = new Random();
    private Timer timer;
    private World world;
    private Label gameOverLabel;
    private Button gameOverButton;
    private boolean gameEnded;


    public Play(SpriteBatch batch) {
        super(batch);
        Button button;
        Table layout = new Table();
        layout.setFillParent(true);
        Image img;
        img = new Image(Assets.get().images.get("/fount/1_font"));
        img.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);
        staticBg.addActor(img);

        button = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("exit"));
        button.addListener(new ScreenTraveler("Menu"));
        layout.add().expandX();
        layout.add(button).row();
        layout.add().colspan(2).expand();
        ui.addActor(layout);

        gameOverLabel = new Label("GAME OVER", new Label.LabelStyle(Assets.get().fonts.get("blue shaded"), null));
        gameOverLabel.setPosition(Values.WORLD_WIDTH/2, Values.WORLD_HEIGHT/2, Align.center);
        gameOverButton = new ImageButton((ImageButton.ImageButtonStyle) Assets.get().buttonStyles.get("exit"));
        gameOverButton.setPosition(Values.WORLD_WIDTH/2, gameOverLabel.getY() - 2*button.getHeight());
        gameOverButton.addListener(new ScreenTraveler("Menu"));
        gameEnded = false;

    }

    public void show() {
        world = new World(stage.getViewport(), stage.getBatch());
        stage = world;
        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   Obstacle obstacle = new Obstacle(
                                           Assets.get().images.get("/pregrads/Kaktus"),
                                           world.getPlayer().getX() + Values.WORLD_WIDTH,
                                           Values.ground+30
                                   );
                                   obstacle.setSize(obstacle.getWidth() + r.nextInt()%10, obstacle.getHeight() + r.nextInt()%10);
                                   world.addObstacle(obstacle);
                               }
                           },
                0.5f,
                1.5f
        );
        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   Obstacle obstacle = new Obstacle(
                                           Assets.get().images.get("/pregrads/Stena"),
                                           world.getPlayer().getX() + Values.WORLD_WIDTH,
                                           Values.ground+30
                                   );
                                   obstacle.setSize(obstacle.getWidth() + r.nextInt()%10, obstacle.getHeight() + r.nextInt()%10);
                                   world.addObstacle(obstacle);
                               }
                           },
                35f,
                5.2f
        );
        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   world.addCloud(
                                           new Obstacle(
                                                   Assets.get().images.get("/prochee/i"),
                                                   world.getPlayer().getX() + Values.WORLD_WIDTH,
                                                   Values.WORLD_HEIGHT - Values.ground + r.nextInt() % 20
                                           )
                                   );
                               }
                           },
                0.2f,
                1f
        );
        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   world.addCloud(
                                           new Obstacle(
                                                   Assets.get().images.get("/prochee/Cloudy"),
                                                   world.getPlayer().getX() + Values.WORLD_WIDTH,
                                                   Values.WORLD_HEIGHT - Values.ground + r.nextInt() % 20
                                           )
                                   );
                               }
                           },
                0.7f,
                1f
        );

        timer.start();
        super.show();
    }

    public void hide() {
        super.hide();
        timer.stop();
        gameOverLabel.remove();
        gameOverButton.remove();
    }

    protected void postAct() {
        world.getCamera().position.x = world.getPlayer().getX() + Values.WORLD_WIDTH / 2 - 100;
        //world.getCamera().position.y = world.getPlayer().getY();
    }

    public void render(float delta){
        if (!world.isOver()) {
            super.render(delta);
        }else{
            if (!gameEnded) {
                timer.stop();
                ui.addActor(gameOverLabel);
                ui.addActor(gameOverButton);
            }
            ui.act();
            draw();
        }
    }
}
